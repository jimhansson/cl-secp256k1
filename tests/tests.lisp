
(in-package #:secp256k1.tests)


;; debug callbacks
(define-callback error-fn (msg data)
  (format t "ERROR msg: ~a data: ~a~%" msg data))

(define-callback illegal-fn (msg data)
  (format t "ILLEGAL msg: ~a data: ~a~%" msg data))


;; counting callbacks
(defvar counting-callback-calls 0)

(defun reset-counting-callback ()
  (setf counting-callback-calls 0))

(define-callback counting-callback (msg data)
  (declare (ignore data msg))
  (incf counting-callback-calls))


;; tests
(setq prove:*debug-on-error* t)

(prove:subtest "context"
  (let ((ctx (context-create +context-verify+)))
    (context-set-error-callback ctx 'error-fn 3)
    (context-set-illegal-callback ctx 'illegal-fn "test")
    (ec-pubkey-parse ctx (make-array '(32) :element-type '(unsigned-byte 8) :initial-element #xFF))
    (let ((msg (make-array '(32) :element-type '(unsigned-byte 8) :initial-element #xFF))
	  (seckey (make-array '(32) :element-type '(unsigned-byte 8) :initial-element #xFF)))
      (ecdsa-sign ctx msg seckey))
    (let ((ctx2 (context-clone ctx)))
      (context-set-error-callback ctx2 'error-fn "second ctx")
      (context-set-illegal-callback ctx2 'illegal-fn "second ctx")
      (context-destroy ctx2))
    (context-destroy ctx)))
