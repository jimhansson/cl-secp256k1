
(in-package #:secp256k1)


(defconstant +ec-compressed+   +secp256k1-ec-compressed+
  "Used as option to ec-pubkey-create to get a shorter format of a pubkey")
(defconstant +ec-uncompressed+ +secp256k1-ec-uncompressed+
  "Used as option to ec-pubkey-create to get the long format of a pubkey")

(defun ec-seckey-verify (ctx seckey)
  "Verify an ECDSA secret key.
    Returns: 1: secret key is valid
             0: secret key is invalid
    Args:    ctx: pointer to a context object (cannot be NULL)
    In:      seckey: pointer to a 32-byte secret key (cannot be NULL)"
  (check-type ctx secp256k1-context)
  (cffi:with-foreign-array (seckey_ seckey `(:array :unsigned-char ,(length seckey)))
    (let ((valid (secp256k1-ec-seckey-verify ctx seckey_)))
      (unless valid
        (error "seckey not valid")))))

(defun ec-privkey-tweak-add (ctx seckey tweak)
  "Tweak a private key by adding tweak to it.

   Returns: 0 if the tweak was out of range (chance of around 1 in 2^128 for
            uniformly random 32-byte arrays, or if the resulting private key
            would be invalid (only when the tweak is the complement of the
            private key). 1 otherwise.
   Args:    ctx:    pointer to a context object (cannot be NULL).
   In/Out:  seckey: pointer to a 32-byte private key.
   In:      tweak:  pointer to a 32-byte tweak."
  (check-type ctx secp256k1-context)
  (cffi:with-foreign-array (seckey_ seckey `(:array :unsigned-char ,(length seckey)))
    (cffi:with-foreign-array (tweak_ tweak `(:array :unsigned-char ,(length tweak)))
      (let* ((valid (secp256k1-ec-privkey-tweak-add ctx seckey_ tweak_))
             (seckey (cffi:foreign-array-to-lisp seckey_ `(:array :unsigned-char 32))))
        (if valid
            (subseq seckey 0 32)
            (error "could not tweak privkedy"))))))

(defun ec-privkey-tweak-mul (ctx seckey tweak)
  "Tweak a private key by multiplying it by a tweak.
 
   Returns: 0 if the tweak was out of range (chance of around 1 in 2^128 for
            uniformly random 32-byte arrays, or equal to zero. 1 otherwise.
   Args:   ctx:    pointer to a context object (cannot be NULL).
   In/Out: seckey: pointer to a 32-byte private key.
   In:     tweak:  pointer to a 32-byte tweak."
  (check-type ctx secp256k1-context)
  (cffi:with-foreign-array (seckey_ seckey `(:array :unsigned-char ,(length seckey)))
    (cffi:with-foreign-array (tweak_ tweak `(:array :unsigned-char ,(length tweak)))
      (let ((valid (secp256k1-ec-privkey-tweak-mul ctx seckey_ tweak_))
            (seckey (cffi:foreign-array-to-lisp seckey_ `(:array :unsigned-char 32))))
        (if valid
            (subseq seckey 0 32)
            (error "could not tweak privkey"))))))

(defun ec-privkey-negate (ctx seckey)
  "Negates a private key in place.
  
    Returns: 1 always
    Args:   ctx:        pointer to a context object
    In/Out: seckey:     pointer to the 32-byte private key to be negated (cannot be NULL)"
  (check-type ctx secp256k1-context)
  (cffi:with-foreign-array (seckey_ seckey `(:array :unsigned-char ,(length seckey)))
    (let ((valid (secp256k1-ec-privkey-negate ctx seckey_))
          (seckey (cffi:foreign-array-to-lisp seckey_ '(:array :unsigned-char 32))))
      (if valid
          seckey
          (error "could not negate privkey")))))


(defun ec-pubkey-create (ctx seckey)
  (check-type ctx secp256k1-context)
  (cffi:with-foreign-array (seckey_ seckey `(:array :unsigned-char 32))
    (let* ((pubkey (alloc 'secp256k1-pubkey))
           (valid (secp256k1-ec-pubkey-create ctx pubkey seckey_)))
      (if valid
          pubkey
          (error "could not create pubkey")))))

(defun ec-pubkey-tweak-add (ctx pubkey tweak)
  "Tweak a public key by adding tweak times the generator to it.

   Returns: 0 if the tweak was out of range (chance of around 1 in 2^128 for
            uniformly random 32-byte arrays, or if the resulting public key
            would be invalid (only when the tweak is the complement of the
            corresponding private key). 1 otherwise.
   Args:    ctx:    pointer to a context object initialized for validation
                    (cannot be NULL).
   In/Out:  pubkey: pointer to a public key object.
   In:      tweak:  pointer to a 32-byte tweak."
  (check-type ctx secp256k1-context)
  (cffi:with-foreign-array (tweak_ tweak `(:array :unsigned-char ,(length tweak)))
    (let ((valid (secp256k1-ec-pubkey-tweak-add ctx pubkey tweak_)))
      (if valid
          pubkey
          (error "could not tweak pubkey")))))

(defun ec-pubkey-tweak-mul (ctx pubkey tweak)
  "Tweak a public key by multiplying it by a tweak value.

   Returns: 0 if the tweak was out of range (chance of around 1 in 2^128 for
            uniformly random 32-byte arrays, or equal to zero. 1 otherwise.
   Args:    ctx:    pointer to a context object initialized for validation
                   (cannot be NULL).
   In/Out:  pubkey: pointer to a public key object.
   In:      tweak:  pointer to a 32-byte tweak."
  (check-type ctx secp256k1-context)
  (cffi:with-foreign-array (tweak_ tweak `(:array :unsigned-char ,(length tweak)))
    (let ((valid (secp256k1-ec-pubkey-tweak-mul ctx pubkey tweak_)))
      (if valid
          pubkey
          (error "could not tweak pubkey")))))

(defun ec-pubkey-negate (ctx pubkey)
  "Negates a public key in place.
 
    Returns: 1 always
    Args:   ctx:        pointer to a context object
    In/Out: pubkey:     pointer to the public key to be negated (cannot be NULL)"
  (check-type ctx secp256k1-context)
  ;; look on pubkey-parse see if you reform this into a safer verison without valid
  (cffi:with-foreign-array (pubkey_ pubkey `(:array :unsigned-char ,(length pubkey)))
    (let* ((valid (secp256k1-ec-pubkey-negate ctx pubkey_))
           (pubkey (cffi:foreign-array-to-lisp pubkey_ '(:array :unsigned-char 64))))
      (if valid
          pubkey
          (error "could not negate privkey")))))

(defun ec-pubkey-combine (ctx list-of-pubkeys)
  "Add a number of public keys together.
    Returns: 1: the sum of the public keys is valid.
             0: the sum of the public keys is not valid.
    Args:   ctx:        pointer to a context object
    Out:    out:        pointer to a public key object for placing the resulting public key
                        (cannot be NULL)
    In:     ins:        pointer to array of pointers to public keys (cannot be NULL)
            n:          the number of public keys to add together (must be at least 1)"
  (check-type ctx secp256k1-context)
  (cffi:with-foreign-array (list-pks list-of-pubkeys `(:array 'secp256k1-pubkey))
    (let ((pubkey (alloc 'secp256k1-pubkey)))
      (if (secp256k1-ec-pubkey-combine ctx pubkey list-pks (length list-of-pubkeys))
          pubkey
          (error "failed in combining public keys")))))

(defun ec-pubkey-parse (ctx key)
  (check-type ctx secp256k1-context)
  (cffi:with-foreign-array (key_ key `(:array :unsigned-char ,(length key)))
    (let ((pubkey (alloc 'secp256k1-pubkey)))
      (if (secp256k1-ec-pubkey-parse ctx pubkey key_ (length key))
          pubkey
          (error "could not parse pubkey")))))

(defun ec-pubkey-serialize (ctx pubkey options)
  (check-type ctx secp256k1-context)
  (with-many-alloc ((serialized ':unsigned-char 65)
                    (size ':unsigned-long 1))
    (setf (autowrap:c-aref size 0 :unsigned-long) 65)
    ;; TODO today this will always return 1 but we should add guards that throws.
    (secp256k1-ec-pubkey-serialize ctx serialized size pubkey options)
    (cffi:foreign-array-to-lisp serialized
                                `(:array :unsigned-char ,(autowrap:c-aref size 0 :unsigned-long)))))



