
(in-package #:secp256k1)

(defun ecdsa-sign (ctx msg seckey &optional (noncefp nil) (ndata nil))
  "Create an ECDSA signature.
   Returns: Signature
   Args:    ctx:     pointer to a context object, initialized for signing (cannot be NULL)
            msg32:   the 32-byte message hash being signed (cannot be NULL)
            seckey:  pointer to a 32-byte secret key (cannot be NULL)
            noncefp: pointer to a nonce generation function. 
                     If NULL, secp256k1-nonce-function-default is used
            ndata:   pointer to arbitrary data used by the nonce generation function (can be NULL)

  The created signature is always in lower-S form. See 'ecdsa-signature-normalize for more
  details."
  (check-type ctx secp256k1-context)
  (assert (= 32 (length msg)))
  (assert (= 32 (length seckey)))
  (let ((sig (alloc 'secp256k1-ecdsa-signature)))
    (cffi:with-foreign-array (msg_ msg `(:array :unsigned-char ,(length msg)))
      (cffi:with-foreign-array (seckey_ seckey `(:array :unsigned-char ,(length seckey)))
        (if (= (secp256k1-ecdsa-sign ctx sig msg_ seckey_ noncefp ndata) 1)
            sig
            (error "could not sign message"))))))

(defun ecdsa-verify (ctx sig msg pubkey)
  "Verify an ECDSA signature.

   Returns: 1: correct signature
            0: incorrect or unparseable signature
   Args:    ctx:       a secp256k1 context object, initialized for verification.
            sig:       the signature being verified (cannot be NULL)
            msg32:     the 32-byte message hash being verified (cannot be NULL)
            pubkey:    pointer to an initialized public key to verify with (cannot be NULL)

  To avoid accepting malleable signatures, only ECDSA signatures in lower-S form are
  accepted.

  If you need to accept ECDSA signatures from sources that do not obey this rule, apply
  ecdsa-signature-normalize to the signature prior to validation, but be aware
  that doing so results in malleable signatures.

  For details, see the comments for that function."
  (check-type ctx secp256k1-context)
  (assert (= 32 (length msg)))
  ;; TODO: more asserts
  (cffi:with-foreign-array (msg_ msg `(:array :unsigned-char ,(length msg)))
    (cffi:with-foreign-array (sig_ sig `(:array :unsigned-char ,(length sig)))
      (cffi:with-foreign-array (pubkey_ pubkey `(:array :unsigned-char ,(length pubkey)))
        (secp256k1-ecdsa-verify ctx sig_ msg_ pubkey_)))))

(defun ecdsa-signature-parse-compact (ctx input)
  "Parse an ECDSA signature in compact (64 bytes) format.
 
   Returns: signature when it could be parsed, signal error otherwise
   Args: ctx:      a secp256k1 context object
   In:   input64:  a pointer to the 64-byte array to parse
 
   The signature must consist of a 32-byte big endian R value, followed by a 32-byte big
   endian S value. If R or S fall outside of [0..order-1], the encoding is invalid. R and
   S with value 0 are allowed in the encoding.
 
   After the call, sig will always be initialized. If parsing failed or R or S are zero,
   the resulting sig value is guaranteed to fail validation for any message and public
   key."
  (check-type ctx secp256k1-context)
  (assert (= 64 (length input)))
  (cffi:with-foreign-array (input_ input `(:array :unsigned-char ,(length input)))
    (let ((sig_ (alloc 'secp256k1-ecdsa-signature)))
      (if (= (secp256k1-ecdsa-signature-parse-compact ctx sig_ input_) 1)
          sig_
          (error "could not parse compact signature")))))

(defun ecdsa-signature-parse-der (ctx input)
  "Parse a DER ECDSA signature.
 
   Returns: signature when it could be parsed, signal error otherwise
   Args: ctx:      a secp256k1 context object
   In:   input:    a pointer to the signature to be parsed
 
   This function will accept any valid DER encoded signature, even if the encoded numbers
   are out of range.
 
   After the call, sig will always be initialized. If parsing failed or the encoded
   numbers are out of range, signature validation with it is guaranteed to fail for every
   message and public key."
  (check-type ctx secp256k1-context)
  (cffi:with-foreign-array (input_ input `(:array :unsigned-char ,(length input)))
    (let ((sig_ (alloc 'secp256k1-ecdsa-signature)))
      (if (= (secp256k1-ecdsa-signature-parse-der ctx sig_ input_ (length input)) 1)
          sig_
          (error "could not parse compact signature")))))

(defun ecdsa-signature-serialize-der (ctx sig &optional (max-sixe 100))
  "Serialize an ECDSA signature in DER format.
 
   Returns: return serialized signature, otherwise signal error
   Args:   ctx:       a secp256k1 context object
   In:     sig:       a pointer to an initialized signature object"
  (check-type ctx secp256k1-context)
  (with-many-alloc ((output_ ':unsigned-char max-sixe)
                    (size ':unsigned-long 1))
    (setf (autowrap:c-aref size 0 :unsigned-long) max-sixe)
    (if (= (secp256k1-ecdsa-signature-serialize-der ctx output_ size sig) 1)
        (flet ((get-size-value ()
                 (autowrap:c-aref size 0 :unsigned-long)))
          (cffi:foreign-array-to-lisp output_
                                      `(:array
                                        :unsigned-char
                                        ,(get-size-value))))
        ;; this error might mean the output_ buffer is to small.
        (error "could not serialize signature in der format, do you need a bigger sized buffer?"))))

(defun ecdsa-signature-serialize-compact (ctx sig)
  "Serialize an ECDSA signature in DER format.
 
   Returns: return serialized signature
   Args:   ctx:       a secp256k1 context object
   In:     sig:       a pointer to an initialized signature object"
  (check-type ctx secp256k1-context)
  (with-many-alloc ((output_ ':unsigned-char 64))
    (if (= (secp256k1-ecdsa-signature-serialize-compact ctx output_ sig) 1)
        (cffi:foreign-array-to-lisp output_
                                    '(:array :unsigned-char 64))
        (error "could not serialize signature in compact format"))))
