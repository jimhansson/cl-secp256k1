
(in-package #:secp256k1)

(defvar *callback-counter* 0)
(defvar *callback-args-map* '())

(defconstant +context-verify+ +secp256k1-context-verify+
  "Option for context-create to initialize it for verifying")
(defconstant +context-sign+   +secp256k1-context-sign+
  "Option for context-create to initialize it for signing, this will create some
        lookup-tables that cost some time to create.")


;; wraps body in a named and randomized secp256k1 context.
(defmacro with-context ((name options) &body body)
  "small utility macro that makes it easier to make sure you destroy context:s, but
remember this might really bad for performance."
  (declare (sb-ext:muffle-conditions sb-ext:code-deletion-note))
  `(let ((,name (secp256k1-context-create ,options)))
     (unless (= +context-verify+ ,options)
       (cffi:with-foreign-array (seed_ (ironclad:random-data 32 (ironclad:make-prng :OS))
				       '(:array :unsigned-char 32))
         (secp256k1-context-randomize ,name seed_)))
     (unwind-protect (progn ,@body)
       (secp256k1-context-destroy ,name))))

(defun context-randomize (ctx seed)
  "Updates the context randomization to protect against side-channel leakage.

    Returns: 1: randomization successfully updated or nothing to randomize
             0: error
    Args:    ctx:       pointer to a context object (cannot be NULL)
    In:      seed32:    pointer to a 32-byte random seed (NULL resets to initial state)
  
   While secp256k1 code is written to be constant-time no matter what secret
   values are, it's possible that a future compiler may output code which isn't,
   and also that the CPU may not emit the same radio frequencies or draw the same
   amount power for all values.

   This function provides a seed which is combined into the blinding value: that
   blinding value is added before each multiplication (and removed afterwards) so
   that it does not affect function results, but shields against attacks which
   rely on any input-dependent behaviour.
 
   This function has currently an effect only on contexts initialized for signing
   because randomization is currently used only for signing. However, this is not
   guaranteed and may change in the future. It is safe to call this function on
   contexts not initialized for signing; then it will have no effect and return 1.
  
   You should call this after secp256k1_context_create or
   secp256k1_context_clone (and secp256k1_context_preallocated_create or
   secp256k1_context_clone, resp.), and you may call this repeatedly afterwards."
  (check-type ctx secp256k1-context)
  (check-type seed (array (unsigned-byte 8) 32))
  (cffi:with-foreign-array (seed_ seed '(:array :unsigned-char 32))
    (secp256k1-context-randomize ctx seed_)))

(defun context-create (flags)
  "Create a secp256k1 context object (in dynamically allocated memory).
 
    This function uses malloc to allocate memory. It is guaranteed that malloc is
    called at most once for every call of this function. If you need to avoid dynamic
    memory allocation entirely, see the functions in secp256k1_preallocated.h.
  
    Returns: a newly created context object.
    In:      flags: which parts of the context to initialize.
  
    See also secp256k1_context_randomize."
  (secp256k1-context-create flags))

(defun context-clone (ctx)
  "Copy a secp256k1 context object (into dynamically allocated memory).
  
    This function uses malloc to allocate memory. It is guaranteed that malloc is
    called at most once for every call of this function. If you need to avoid dynamic
    memory allocation entirely, see the functions in secp256k1_preallocated.h.
  
    Returns: a newly created context object.
    Args:    ctx: an existing context to copy (cannot be NULL)"
  (check-type ctx secp256k1-context)
  (secp256k1-context-clone ctx))

(defun context-destroy (ctx)
  "Destroy a secp256k1 context object (created in dynamically allocated memory).
  
    The context pointer may not be used afterwards.
  
    The context to destroy must have been created using secp256k1_context_create
    or secp256k1_context_clone. If the context has instead been created using
    secp256k1_context_preallocated_create or secp256k1_context_preallocated_clone, the
    behaviour is undefined. In that case, secp256k1_context_preallocated_destroy must
    be used instead.
  
    Args:   ctx: an existing context to destroy, constructed using
                 secp256k1_context_create or secp256k1_context_clone"
  (check-type ctx secp256k1-context)
  (secp256k1-context-destroy ctx)
  (flet ((matches-ctx (item) (eq (caar item) ctx)))
    (setq *callback-args-map* (remove-if #'matches-ctx *callback-args-map*))))

(defmacro define-callback (name (msg-var data-var) &body body)
  "Used to create callback functions for use in next two functions

    when setting up callback we have trouble passing the data variable throu,
    without geting it destroyed by GC and handling type information correctly, 
    so instead we pass a handle throu and keep the data on the lisp side. This
    wrapper will turn that handle back into the data. and also convert the msg
    string."
  (let ((counter-sym (gensym)))
    `(cffi:defcallback ,name :void ((,msg-var :pointer) (,data-var :pointer))
       (let* ((,counter-sym (cffi:pointer-address (cffi:convert-from-foreign ,data-var :pointer)))
	      (,data-var (cdr (assoc ,counter-sym *callback-args-map* :key #'cadr)))
	      (,msg-var (cffi:foreign-string-to-lisp ,msg-var)))
	 ,@body))))

(defun context-set-illegal-callback (ctx callback data)
  "Set a callback function to be called when an illegal argument is passed to

    an API call. It will only trigger for violations that are mentioned
    explicitly in the header.
 
    The philosophy is that these shouldn't be dealt with through a
    specific return value, as calling code should not have branches to deal with
    the case that this code itself is broken.
 
    On the other hand, during debug stage, one would want to be informed about
    such mistakes, and the default (crashing) may be inadvisable.
    When this callback is triggered, the API function called is guaranteed not
    to cause a crash, though its return value and output arguments are
    undefined.
 
    When this function has not been called (or called with fn==NULL), then the
    default handler will be used. The library provides a default handler which
    writes the message to stderr and calls abort. This default handler can be
    replaced at link time if the preprocessor macro
    USE_EXTERNAL_DEFAULT_CALLBACKS is defined, which is the case if the build
    has been configured with --enable-external-default-callbacks. Then the
    following two symbols must be provided to link against:
     - void secp256k1_default_illegal_callback_fn(const char* message, void* data);
     - void secp256k1_default_error_callback_fn(const char* message, void* data);
    The library can call these default handlers even before a proper callback data
    pointer could have been set using secp256k1_context_set_illegal_callback or
    secp256k1_context_set_error_callback, e.g., when the creation of a context
    fails. In this case, the corresponding default handler will be called with
    the data pointer argument set to NULL.
  
    Args: ctx:  an existing context object (cannot be NULL)
    In:   fun:  a pointer to a function to call when an illegal argument is
                passed to the API, taking a message and an opaque pointer.
                (NULL restores the default handler.)
          data: the opaque pointer to pass to fun above.
  
    See also secp256k1_context_set_error_callback."
  (check-type ctx secp256k1-context)
  (incf *callback-counter*)
  (setf *callback-args-map*
	(acons `(,ctx ,*callback-counter*) data *callback-args-map*))
  (secp256k1-context-set-illegal-callback
   ctx
   (autowrap:callback callback)
   (cffi:make-pointer *callback-counter*)))

(defun context-set-error-callback (ctx callback data)
  "Set a callback function to be called when an internal consistency check
    fails. The default is crashing.
  
    This can only trigger in case of a hardware failure, miscompilation,
    memory corruption, serious bug in the library, or other error would can
    otherwise result in undefined behaviour. It will not trigger due to mere
    incorrect usage of the API (see secp256k1_context_set_illegal_callback
    for that). After this callback returns, anything may happen, including
    crashing.
  
    Args: ctx:  an existing context object (cannot be NULL)
    In:   fun:  a pointer to a function to call when an internal error occurs,
                taking a message and an opaque pointer (NULL restores the
                default handler, see secp256k1_context_set_illegal_callback
                for details).
          data: the opaque pointer to pass to fun above.
  
    See also secp256k1_context_set_illegal_callback."
  (check-type ctx secp256k1-context)
  (incf *callback-counter*)
  (setq *callback-args-map*
	(acons `(,ctx ,*callback-counter*) data *callback-args-map*))
  (secp256k1-context-set-error-callback
   ctx
   (autowrap:callback callback)
   (cffi:make-pointer *callback-counter*)))
