;;;; secp256k1.lisp

(in-package #:secp256k1)


(defun init ()

  (cffi:define-foreign-library secp256k1
    (t (:default "libsecp256k1")))
  
  (cffi:use-foreign-library secp256k1))


(init)

(defun deinit ()
  (cffi:close-foreign-library secp256k1))
