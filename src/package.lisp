;;;; package.lisp
(defpackage #:secp256k1.ffi
  (:documentation "This is the raw FFI package around libsecp256k1, it
exactly mirrors the C Library. Avoid using these things directly
instead use the wrapping package instead as much as possible.")
  ;; really need to clean up what we are exporting here.
  (:export #:+NULL+
           #:+SECP256K1-API+
           #:+SECP256K1-CONTEXT-NONE+
           #:+SECP256K1-CONTEXT-SIGN+
           #:+SECP256K1-CONTEXT-VERIFY+
           #:+SECP256K1-EC-COMPRESSED+
           #:+SECP256K1-EC-UNCOMPRESSED+
           #:+SECP256K1-FLAGS-BIT-COMPRESSION+
           #:+SECP256K1-FLAGS-BIT-CONTEXT-SIGN+
           #:+SECP256K1-FLAGS-BIT-CONTEXT-VERIFY+
           #:+SECP256K1-FLAGS-TYPE-COMPRESSION+
           #:+SECP256K1-FLAGS-TYPE-CONTEXT+
           #:+SECP256K1-FLAGS-TYPE-MASK+
           #:+SECP256K1-H+
           #:+SECP256K1-INLINE+
           #:+SECP256K1-TAG-PUBKEY-EVEN+
           #:+SECP256K1-TAG-PUBKEY-HYBRID-EVEN+
           #:+SECP256K1-TAG-PUBKEY-HYBRID-ODD+
           #:+SECP256K1-TAG-PUBKEY-ODD+
           #:+SECP256K1-TAG-PUBKEY-UNCOMPRESSED+
           #:+SECP256K1-WARN-UNUSED-RESULT+
           #:+_PTRDIFF_T+
           #:+_SIZE_T+
           #:+_WCHAR_T+
           #:+__CLANG_MAX_ALIGN_T_DEFINED+
           #:+__STDDEF_H+
           #:MAX-ALIGN-T
           #:MAX-ALIGN-T.__CLANG_MAX_ALIGN_NONCE1
           #:MAX-ALIGN-T.__CLANG_MAX_ALIGN_NONCE1&
           #:PTRDIFF-T
           #:SECP256K1-CONTEXT
           #:SECP256K1-CONTEXT-CLONE
           #:SECP256K1-CONTEXT-CREATE
           #:SECP256K1-CONTEXT-DESTROY
           #:SECP256K1-CONTEXT-RANDOMIZE
           #:SECP256K1-CONTEXT-SET-ERROR-CALLBACK
           #:SECP256K1-CONTEXT-SET-ILLEGAL-CALLBACK
           #:SECP256K1-CONTEXT-STRUCT
           #:SECP256K1-EC-PRIVKEY-NEGATE
           #:SECP256K1-EC-PRIVKEY-TWEAK-ADD
           #:SECP256K1-EC-PRIVKEY-TWEAK-MUL
           #:SECP256K1-EC-PUBKEY-COMBINE
           #:SECP256K1-EC-PUBKEY-CREATE
           #:SECP256K1-EC-PUBKEY-NEGATE
           #:SECP256K1-EC-PUBKEY-PARSE
           #:SECP256K1-EC-PUBKEY-SERIALIZE
           #:SECP256K1-EC-PUBKEY-TWEAK-ADD
           #:SECP256K1-EC-PUBKEY-TWEAK-MUL
           #:SECP256K1-EC-SECKEY-VERIFY
           #:SECP256K1-ECDSA-SIGN
           #:SECP256K1-ECDSA-SIGNATURE
           #:SECP256K1-ECDSA-SIGNATURE-NORMALIZE
           #:SECP256K1-ECDSA-SIGNATURE-PARSE-COMPACT
           #:SECP256K1-ECDSA-SIGNATURE-PARSE-DER
           #:SECP256K1-ECDSA-SIGNATURE-SERIALIZE-COMPACT
           #:SECP256K1-ECDSA-SIGNATURE-SERIALIZE-DER
           #:SECP256K1-ECDSA-SIGNATURE.DATA[]
           #:SECP256K1-ECDSA-SIGNATURE.DATA[]&
           #:SECP256K1-ECDSA-VERIFY
           #:SECP256K1-NONCE-FUNCTION
           #:SECP256K1-NONCE-FUNCTION-DEFAULT
           #:SECP256K1-NONCE-FUNCTION-RFC6979
           #:SECP256K1-PUBKEY
           #:SECP256K1-PUBKEY.DATA[]
           #:SECP256K1-PUBKEY.DATA[]&
           #:SECP256K1-SCRATCH-SPACE
           #:SECP256K1-SCRATCH-SPACE-CREATE
           #:SECP256K1-SCRATCH-SPACE-DESTROY
           #:SECP256K1-SCRATCH-SPACE-STRUCT
           #:SIZE-T
           #:WCHAR-T))

(defpackage #:secp256k1
  (:documentation "This is nice wrapper around libsecp256k1, The major
changes from the FFI package is that these functions have been tweaked
abit. they don't return any FFI pointers. They don't return error
codes, instead they will singal errors. They don't take arguments
that are about sizes of buffers, we know the sizes and don't need
them. Other than that these functions also have shorter names, by
droping the prefix secp256k1.")
  (:use #:cl
        #:autowrap.minimal
        #:plus-c
        #:secp256k1.ffi)
  (:export ;;global flags
           #:+context-verify+
           #:+context-sign+
           #:+ec-compressed+
           #:+ec-uncompressed+
	   ;; functions
	   #:init
	   #:deinit
           ;; context functions
           #:with-context
           #:context-create
           #:context-clone
           #:context-destroy
	   #:define-callback
           #:context-set-illegal-callback
           #:context-set-error-callback
           ;; private key functions
           #:ec-privkey-tweak-add
           #:ec-privkey-tweak-mul
           #:ec-privkey-negate
           #:ec-seckey-verify
           ;: public key functions
           #:ec-pubkey-create
           #:ec-pubkey-tweak-add
           #:ec-pubkey-tweak-mul
           #:ec-pubkey-negate
           #:ec-pubkey-parse
           #:ec-pubkey-serialize
           ;; ecdsa functions
           #:ecdsa-sign
           #:ecdsa-verify
           #:ecdsa-signature-parse-compact
           #:ecdsa-signature-parse-der
           #:ecdsa-signature-serialize-der
           #:ecdsa-signature-serialize-compact))

