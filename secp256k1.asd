;;;; secp256k1.asd
(defpackage :secp256k1.asdf
  (:use #:cl #:asdf))

(in-package :secp256k1.asdf)

#|
;; should it be non-propeagating-operation instead ?
(defclass doc-op (asdf:selfward-operation)
  ()
  (:documentation "An ASDF operation that will extracts documentation"))

#|
(defmethod asdf:perform :before ((o doc-op) (c component)) nil)
(defmethod asdf:perform ((o doc-op) (c component)) nil)
|#

(defmethod asdf:perform :before ((o doc-op) (c system))
  (format t "Creating documentation for component ~s~%" (component-name c)))

(defmethod asdf:perform ((o doc-op) (c system))
  ;; here i can have a default behaviour and if a system does not like it can specialize
  ;; it by using :perform in the defsystem.
  (net.didierverna.declt:declt (component-name c)))
|#

(asdf:defsystem #:secp256k1
  :description "A wrapper around the secp256k1 eliptic curve library from bitcoin-core"
  :author "Jim Hansson <jim.hansson@gmail.com>"
  :license  "MIT License"
  :version "0.0.2"
  :depends-on (:cl-autowrap
	       :cl-plus-c
	       :ironclad) ; TODO: why do we depend on ironclad here, is it only for the
  ;; random salt in create context?
  :pathname "src"
  :serial t
  :components ((:file "package")
	       (:file "autowrap")
               (:file "secp256k1")
	       (:file "context")
	       (:file "ec")
	       (:file "ecdsa")
	       (:module #:autospec
			:pathname "autospec"
			:components ((:static-file "secp256k1.h"))))
  ;;:long-description #. (uiop:read-file-string (uiop:subpathname *load-pathname* "README.md"))
  :in-order-to ((asdf:test-op
		 (asdf:test-op :secp256k1.tests))))

(asdf:defsystem #:secp256k1.tests
  :description "Test package for the secp256k1 library wrapper"
  :author "Jim Hansson <jim.hansson@gmail.com>"
  :license  "MIT License"
  :version "0.0.2"
  :depends-on (:secp256k1
	       :prove)
  :pathname "tests"
  :serial t
  :components ((:file "package")
	       (:file "tests"))
  :perform (asdf:test-op (op c)
			 (progn
			   (funcall (intern #.(string :run) :prove) c))))
